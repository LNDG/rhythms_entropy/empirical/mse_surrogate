%% Plot overview of age differences

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
    mseOriginal = load([pn.root, 'B_data/E_mseMerged.mat'], 'mseMerged', 'IDs');

    pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/A2_MSE_phaseShuffled/';
    mseShuffled = load([pn.root, 'B_data/E_mseMerged.mat'], 'mseMerged', 'IDs');
    
    addpath([pn.root, 'T_tools/fieldtrip-20170904']); ft_defaults;

    load([pn.root, 'B_data/F_CBPA_originalVSshuffled.mat'], 'stat', 'methodLabels')

%% FIGURE 5: combination plot of mean traces + stats

    pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

    indCond = 1; channels = 1:60;
    indCondOrig = 2;
    
    h = figure('units','normalized','position',[0 0 .3 .7]);
    set(0,'DefaultAxesColor',[1 1 1])
    subplot(4,2,1); cla; hold on;
        curAverage = nanmean(mseOriginal.mseMerged{1,indCondOrig}.MSEVanilla(:, channels,:)./mseShuffled.mseMerged{1,indCond}.MSEVanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseOriginal.mseMerged{2,indCondOrig}.MSEVanilla(:, channels,:)./mseShuffled.mseMerged{2,indCond}.MSEVanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); title({'Original'; ''});
        ylim([0.93 1]);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
        legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}); legend('boxoff');
    subplot(4,2,2); hold on;
        curAverage = nanmean(mseOriginal.mseMerged{1,indCondOrig}.MSEfiltskip(:, channels,:)./mseShuffled.mseMerged{1,indCond}.MSEfiltskip(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseOriginal.mseMerged{2,indCondOrig}.MSEfiltskip(:, channels,:)./mseShuffled.mseMerged{2,indCond}.MSEfiltskip(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); title({'Low-pass'; ''});
        ylim([0.93 1]);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
     subplot(4,2,5); hold on;
        curAverage = nanmean(mseOriginal.mseMerged{1,indCondOrig}.Rvanilla(:, channels,:)./mseShuffled.mseMerged{1,indCond}.Rvanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseOriginal.mseMerged{2,indCondOrig}.Rvanilla(:, channels,:)./mseShuffled.mseMerged{2,indCond}.Rvanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); %title({'Vanilla (r = .5, m = 2)'; ''});
        ylim([.99 1.01]);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Similarity Bounds');
        %legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}); legend('boxoff');
    subplot(4,2,6); hold on;
        curAverage = nanmean(mseOriginal.mseMerged{1,indCondOrig}.Rfiltskip(:, channels,:)./mseShuffled.mseMerged{1,indCond}.Rfiltskip(:, channels,:),2);
        %figure; imagesc(squeeze(curAverage))
        standError = nanstd(squeeze(curAverage),1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseOriginal.mseMerged{2,indCondOrig}.Rfiltskip(:, channels,:)./mseShuffled.mseMerged{2,indCond}.Rfiltskip(:, channels,:),2);
        standError = nanstd(squeeze(curAverage),1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); %title({'Lowpass + scale-wise R'; ''});
        ylim([.99 1.01]);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Similarity Bounds');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'FX_MSE_R_byMethod';

%% plot EO: OA vs. YA MSE CBPA

    indContrast = 1;
   
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/altmany-export_fig-4282d74')

    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    colormap(cBrew)
    
    for indMethod = 1:4
        if indMethod>2
            subplot(4,2,2*2+indMethod); cla;
        else
            subplot(4,2,2+indMethod); cla;
        end
        caxis([-6 6]) 
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', .4); 
        hold on;
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', stat{indContrast,indMethod}.mask); 
        set(gca, 'XTick', [1:8:35, 42]);
        freqContent = fliplr((1./[1:42])*250);
        set(gca, 'XTickLabels', round(freqContent(get(gca, 'XTick')),0));
        set(gca, 'XDir', 'reverse')
        set(gca, 'YTick', []);
        xlabel('Scale (Hz)'); ylabel({'Channels'; 'ant.-posterior'});
        if indMethod == 2 || indMethod == 4
            cb = colorbar('location', 'EastOutside'); set(get(cb,'label'),'string','t values');
        end
        %title(labels(indMethod))
    end

    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    pn.plotFolder = '/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/A2_MSE_phaseShuffled/C_figures/';
    figureName = 'F2_statsRatio';
    set(h, 'PaperSize', [80,40]);
    print(h, '-dpdf', [pn.plotFolder, figureName, '.pdf'], '-bestfit')
    print(h, '-dpdf', [pn.plotFolder, figureName, '.pdf'])
    export_fig([pn.plotFolder, figureName, '.eps'], '-transparent')
    saveas(h, [pn.plotFolder, figureName], 'eps');
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'png');
