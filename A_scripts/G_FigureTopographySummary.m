%% create overview figure of entropy, power and PSD slope age differences

    pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/A2_MSE_phaseShuffled/';

    addpath([pn.root, 'T_tools/brewermap']) % add colorbrewer

    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);

    addpath([pn.root, 'T_tools/']) % add convertPtoExponential

%% FIGURE 4B-D: plot topographies of MSE age differences

    addpath([pn.root, 'T_tools/fieldtrip-20170904']); ft_defaults;
    load([pn.root, 'B_data/F_CBPA_originalVSshuffled.mat'], 'stat', 'methodLabels')
    contrastLabels = {'EO: OA vs. YA'};

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.style = 'both';
    cfg.colormap = cBrew;

%% plot topography of coarse-scale entropy difference

    % Note that the internal FieldTrip cluster statistics reorder the entropy
    % timescales according to increasing frequency.

    h = figure('units','normalized','position',[.1 .1 .7 .45]);
    set(h, 'renderer', 'Painters')
    subplot(2,3,1)
        cfg.marker = 'off';  
%         cfg.highlight = 'yes';
%         cfg.highlightchannel = stat{1,1}.label(max(stat{1,1}.negclusterslabelmat(:,1:5)==1,[],2));
%         cfg.highlightcolor = [1 1 0];
%         cfg.highlightsymbol = '.';
%         cfg.highlightsize = 18;
        cfg.zlim = [-5 5];
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat{1,1}.stat(:,1:5),2));
        ft_topoplotER(cfg,plotData);
        cb = colorbar; set(get(cb,'title'),'string','t values');
        pval = []; pval = convertPtoExponential(stat{1,1}.negclusters(1).prob);
        title({'Coarse scale entropy OA minus YA', ['p = ', pval{1}]});
%% plot topography of fine-scale entropy difference
    
    subplot(2,3,2)
        cfg.marker = 'off';  
%         cfg.highlight = 'yes';
%         cfg.highlightchannel = stat{1,1}.label(max(stat{1,2}.posclusterslabelmat(:,38:42),[],2));
%         cfg.highlightcolor = [1 1 0];
%         cfg.highlightsymbol = '.';
%         cfg.highlightsize = 18;
        cfg.zlim = [-5 5];
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat{1,2}.stat(:,38:42),2));
        ft_topoplotER(cfg,plotData);
        cb = colorbar; set(get(cb,'title'),'string','t values');
        pval = []; pval = convertPtoExponential(stat{1,2}.posclusters(1).prob);
        title({'Fine scale entropy OA minus YA', ['p = ', pval{1}]});
     